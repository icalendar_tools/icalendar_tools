#!/usr/bin/env python

"""
Some tools to work with ics based calendar files.

Unless otherwise selected this tool does not overwrite existing files. Output
is written on stdout and should be redirected. Logging messages go to stderr.

This module offers some hopefully helpful functions for handling ics calendars
based on the 'icalendar' and 'pytz' libraries. These function can be loaded as
python objects and used in other scripts.

When using the standard methods this program tries to apply some repairing and
normalizing to get a sane format to start with, i.e. proper timezone with DST
information for every event entry to prevent some duplicates on merging.
"""

# Ideas for improvement:
#  - use fuzzy text matching, e.g.:
#    sum1 = 'This is one "summary" line'
#    sum2 = 'This is one \\"summary\\" line'
#    difflib.SequenceMatcher(None, sum1, sum2).ratio()
#    --> return high ratio because probably it's meant to be the same, right?

from icalendar import Calendar, Event
import logging
import pytz
import sys


# probably the ics file is encoded in this
ENCODING = 'iso-8859-15'

# Default to revert to if no timezone is set for any event
LOCAL_TIMEZONE = 'Europe/Berlin'
pytz_local = pytz.timezone(LOCAL_TIMEZONE)
pytz_utc = pytz.timezone('UTC')


def remove_redundant_quote_escapes(string):
    # google calendar seems to remove useles additional escapes, remove them for
    # hashing from all elements:
    string = string.replace('\\"', '"')
    return string

def entry_hash(comp):
    """hash value of a ical calendar event entry or other types

       For events the hash is based on the datetime object of the start time
       as well as the summary line. For other types of entries the hash is
       based on the string representation of the whole object, i.e. more
       strict.
    """
    try:
        summary = remove_redundant_quote_escapes(comp['summary'])
        return hash(comp['dtstart'].dt) + hash(summary)
    except (KeyError, AttributeError):
        logging.debug("non-event component: %s --> %s" % (type(comp), comp))
        return hash(str(comp))

def entry_str(comp):
    """a bit nicer string representation for output usage"""
    try:
        s = "entry: %s, %s" % (comp['dtstart'].dt, comp['summary'])
    except KeyError:
        s = "entry with type: %s" % type(comp)
        return s.encode(ENCODING)

def repair_datetime_with_broken_dst(dt):
    """making sure DST is correctly used in the datetime object"""
    if not dt.tzinfo:
        # astimezone() cannot be applied to a naive datetime, assuming local timezone
        logging.debug("datetime object without timezone information, replacing by local timezone: %s" % dt)
        dt = dt.replace(tzinfo=pytz_local)
    dt = dt.tzinfo.localize(dt.replace(tzinfo=None))
    return dt

def ical_datetime_to_tz(dt, timezone='UTC'):
    """for an ical-based datetime object apply a different timezone if possible"""
    try:
        new_dt = repair_datetime_with_broken_dst(dt)
        new_dt = new_dt.astimezone(pytz.timezone(timezone))
        logging.debug("%s --> %s" % (dt, new_dt))
        return new_dt
    except AttributeError:
        # 'datetime.date' object has no attribute 'astimezone' but does not
        # need to as the timezone shouldn't matter
        return dt

def ical_datetime_to_utc(dt):
    """for an ical-based datetime object apply the UTC timezone"""
    return ical_datetime_to_tz(dt, timezone='UTC')

def ical_datetime_to_local(dt, timezone='Europe/Berlin'):
    """for an ical-based datetime object apply the local timezone"""
    return ical_datetime_to_tz(dt, timezone)

def file_to_cal(filename):
    """readin ics-calendar from file using filename"""
    cal = Calendar.from_ical(open(filename, "rb").read())
    return cal

def file_to_cal_with_repair(filename):
    """readin ics-calendar from file using filename and repair broken timestamps"""
    cal = file_to_cal(filename)
    for i in cal.walk():
        for key in i.keys():
            try:
                dt = i[key].dt
                logging.debug("found timestamp: %s" % dt)
                dt = repair_datetime_with_broken_dst(dt)
            except AttributeError:
                pass

    return cal

def nice_event(event):
    """return a 'nice' representation for overview purposes of an ics event

    This gives the starting datetime in UTC as well as the summary line as a
    tuple which can be easily converted to strings.
    """
    dt = ical_datetime_to_utc(event['dtstart'].dt)
    summary = remove_redundant_quote_escapes(event['summary']).encode(ENCODING)
    return (dt, summary)

def nice_event_set(cal):
    """using 'nice_event()' to create a (duplicate-free) set of event overview"""
    events = [event for event in cal.walk('vevent')]
    events_with_dtstart_and_summary = [event for event in events if 'dtstart' in event and 'summary' in event]
    # generate nice overview list
    events_list = ["%s: %s" % nice_event(event) for event in events_with_dtstart_and_summary]
    # a set gives us a duplicate free collection
    events_set = set(events_list)
    return events_set


def remove_duplicates(cal):
    """Copy the input calendar and return a new one without duplicates"""
    # create new empty calendar
    new_cal = Calendar()
    # copy only main attributes not all subcomponents, i.e. events and stuff
    new_cal.update(cal)
    # a set gives us a duplicate free collection
    component_set = set()
    all = len(cal.subcomponents)
    skipped = 0
    for comp in cal.subcomponents:
        component_hash = entry_hash(comp)
        if component_hash in component_set:
            logging.info("skipping duplicate %s" % entry_str(comp))
            skipped += 1
            continue
        else:
            component_set.add(component_hash)
        logging.debug("Adding component: %s" % comp)
        new_cal.add_component(comp)
    logging.info("remove_duplicates: all: %u, skipped: %u (%.2f)" % (all, skipped, skipped*1.0/all))
    return new_cal

def add_missing_elements_from_one_calendar_to_another_without_updates(cal, other_cal, interactive=False):
    print("is interactive on? --> %s" % 'yes' if interactive else 'no')
    cal_hash_set = set(entry_hash(i) for i in cal.subcomponents)
    all_old = len(cal.subcomponents)
    all_other = len(other_cal.subcomponents)
    skipped = 0
    added = 0
    for comp in other_cal.subcomponents:
        component_hash = entry_hash(comp)
        if component_hash in cal_hash_set:
            logging.debug("skipping already existing entry %s" % entry_str(comp))
            skipped += 1
            continue
        else:
            cal_hash_set.add(component_hash)
        try:
            logging.debug("Adding component from other to current: %s, %s" % nice_event(comp))
        except KeyError:
            logging.debug("Adding non-event component from other to current")
        if not interactive:
            cal.add_component(comp)
            added += 1
        else:
            user_input = ''
            questions = " -- (y)es (default), (n)o, (s)how complete event?: "
            pre_text = ''
            while True:
                try:
                    pre_text = "Add event %s: %s" % nice_event(comp)
                except KeyError:
                    pre_text = "Add non-event %s" % comp
                user_input = raw_input(pre_text + questions)
                if user_input.startswith('y') or user_input is '':
                    cal.add_component(comp)
                    added += 1
                    break
                elif user_input.startswith('s'):
                    print("----")
                    print("%s" % comp)
                    print("----")
                elif user_input.startswith('n'):
                    skipped += 1
                    break

    logging.info("add_missing_elements_from_one_calendar_to_another_without_updates: all_old: %u, all_other: %u, skipped: %u, added: %u" % (all_old, all_other, skipped, added))

def differences_between_two_cals(cal1, cal2):
    # find values that are only in one of the two lists, not both
    nice_event_set_cal1 = nice_event_set(cal1)
    nice_event_set_cal2 = nice_event_set(cal2)
    crosssection = nice_event_set_cal1 ^ nice_event_set_cal2
    logging.info("differences_between_two_cals: len(cal1): %u, len(cal2): %u, differences: %u" % (len(nice_event_set_cal1), len(nice_event_set_cal2), len(crosssection)))
    return crosssection

def main():
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-i", "--inplace",
            help="Work on input file", action="store_true")
    parser.add_option("-f", "--file", help="Input file path")
    parser.add_option("--duplicates",
            help="Find and remove duplicates", action="store_true")
    parser.add_option("--add_missing",
            help="Add missing elements from one calendar to another one without updates, other as argument")
    parser.add_option("--compare",
            help="Show differences between two calendars in overview, other as argument")
    parser.add_option("--repair_times",
            help="Repair time and date fields if missing timezone or DST broken",
            action="store_true")
    parser.add_option("-?", action="store_true",
        help="Show this help", dest="help")
    parser.add_option("-v", "--verbose",
        help="""Increase verbosity level, specify multiple times to increase
verby""",
        action="count", dest="verbose", default=1)
    parser.add_option("-I", "--interactive",
            help="Work interactively, ask for every change",
            action="store_true")
    parser.add_option("-q", "--quiet", help="Don't show any messages at all", action="store_const", const=0, dest="verbose")

    (options, args) = parser.parse_args()
    if options.help or not (options.file or len(args) > 0):
        print(__doc__)
        parser.print_help()
        sys.exit()
    verbose_to_log = {
        0: logging.CRITICAL,
        1: logging.ERROR,
        2: logging.WARN,
        3: logging.INFO,
        4: logging.DEBUG
    }
    level = logging.CRITICAL
    if options.verbose > 4:
        level = logging.DEBUG
    elif options.verbose < 0:
        level = logging.CRITICAL
    else:
        level = verbose_to_log[options.verbose]
    logging.getLogger().setLevel(level)


    input_filename = options.file if options.file else args[0]
    other = ''
    assert(input_filename)
    # TODO implement "inplace"
    if options.duplicates:
        print(remove_duplicates(file_to_cal(input_filename)).to_ical())
    elif options.add_missing:
        cal = file_to_cal(input_filename)
        other_cal = file_to_cal(options.add_missing)

        add_missing_elements_from_one_calendar_to_another_without_updates(cal,
                other_cal, options.interactive)
        cal = remove_duplicates(cal)
        print(cal.to_ical())
    elif options.compare:
        diffs = differences_between_two_cals(file_to_cal(input_filename), file_to_cal(options.compare))
        import pprint
        pprint.pprint(diffs)
    elif options.repair_times:
        print(file_to_cal_with_repair(input_filename).to_ical())
    else:
        print(__doc__)
        parser.print_help()

if __name__ == "__main__":
    main()
